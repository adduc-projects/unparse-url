<?php

/** @todo consider switching to PHPUnit instead of hand-coded harness */

require __DIR__ . '/functions.php';

$failed = false;

$test = function($input, $expected) use (&$failed) {
    $result = unparse_url($input);
    if ($result !== $expected) {
        echo "Failed!\nExpected: '{$expected}'; Receieved: '{$result}'\nInput: ";
        print_r($input);
        $failed = true;
    }
};

$test([], '');

$url = "https://www.google.com";
$test(parse_url($url), $url);

die($failed ? 1 : 0);
